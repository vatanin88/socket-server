<?php

namespace socketServer;

class server {
    
    public static $connects = array();
    public static $users = array();
    public static $games = array();
    
    private static $local_socket = "tcp://0.0.0.0:2100";
    
    public static function sendMessage($connect, $name, $params) {
        if (!$params) {
            return;
        }
        
        $message = $name ."::".json_encode($params, JSON_UNESCAPED_UNICODE)."\r\n";
        fwrite($connect, $message);
    }

    public function __construct() {
        
    }    
    
    public function run() {
        $socket = stream_socket_server(self::$local_socket, $errno, $errstr);

        if (!$socket) {
            die("$errstr ($errno)\n");
        }
        
        logger::log("Server start");
        
        while (true) {
            $read = self::$connects;
            $read [] = $socket;
            $except = null;

            if (!stream_select($read, self::$connects, $except, null)) {
                break;
            }

            //Новое соеденение
            if (in_array($socket, $read)) {
                $connect = stream_socket_accept($socket, -1);
                
                user::createConnect($connect);
                unset($read[array_search($socket, $read)]);
            }

            

            //Здесь обрабатываем подключенных пользавателей             
            foreach (self::$games as &$game) {
                $game->work();
            }
            
            //читаем что прислали
            foreach (self::$users as &$user) {
                $user->read();
            }
        }
        
        logger::log("Server stop");

        fclose($socket);
    }  
    
    
}