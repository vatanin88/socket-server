<?php

namespace socketServer;

class user {

    private static $tableName = "user";
    private static $questionTableName = 'question';
    private static $answerTableName = 'answer';
    private static $likeTableName = 'like';
    public $ansewrs = array();
    public $connect;
    public $model;
    public $game;
    public $question;
    public $questionId;
    public $likes = array();

    public static function createConnect($connect) {

        $data = fread($connect, 100000);
        if (!$data) {
            return false;
        }

        if (!$params = self::decodeParams($data)) {
            fclose($connect);
            return false;
        }

        if ($params[0] == "setsession") {
            \ORM::reset_db();
            $model = \ORM::for_table(self::$tableName)->where("userId", $params[1]['Session'])->find_one();
            if ($model) {
                $user = new user($connect, $params, $model);

                if (!isset(server::$users[$user->model->userId])) {
                    server::$users[$user->model->userId] = $user;
                    logger::log("User #{$user->model->userId} is connected", $connect, 2);
                } else {
                    $messageArray = array(
                        "nameMethod" => "setsession",
                        "errorMessage" => "User #{$user->model->userId} alredy connected"
                    );
                    server::sendMessage($connect, 'error', $messageArray);
                    logger::log("User #{$this->model->userId} alredy connected", $connect, 1);
                    fclose($connect);
                    return false;
                }
            } else {
                $messageArray = array(
                    "nameMethod" => "setsession",
                    "errorMessage" => "User {$params[1]['Session']} not found"
                );
                server::sendMessage($connect, 'error', $messageArray);
                logger::log("User {$params[1]['Session']} not found", $connect, 1);
                fclose($connect);
                return false;
            }
        }
    }

    public function __construct(&$connect, $params, $model) {
        server::$connects[] = &$connect;
        $this->connect = &$connect;
        $this->model = $model;
        $this->question = $params[1]['question'];
        game::createOrFindGame($this);
    }
    
    public function read() {
        stream_set_blocking ( $this->connect ,0 );
        $data = fread($this->connect, 100000);
        stream_set_blocking ( $this->connect ,1);
        if (!$data) {
            return false;
        }
        
        if (!$params = self::decodeParams($data)) {
            return false;
        }
        
        switch ($params[0]) {
            case 'cancelSession' : 
                $this->game->deleteUser($this);
                break;
            case 'answer' :
                $this->game->answer($this,$params);
                break;
            case 'likeUserAnswer' :
                $this->game->likeUserAnswer($this,$params);
                break;
            default :
                break;
        }
    }
    
    
    public function saveQuestion() {        
         $question = \ORM::for_table(self::$questionTableName)->create();
         $question->question = $this->question;
         $question->game = $this->game->model->id;
         $question->user = $this->model->id;
         $question->save();
         $this->questionId = $question->id;
    }
    
    
    public function saveAnswerAndLikes() {
        
        foreach ($this->ansewrs as $userId => $answer) { 
            
            if (!isset(server::$users[$userId])) {
                continue;
            }
            
            $model = \ORM::for_table(self::$answerTableName)->create();
            $model->question = server::$users[$userId]->questionId;
            $model->game = $this->game->model->id;
            $model->answer = $answer;
            $model->user = $this->model->id;
            $model->save();
        }
        
        foreach ($this->likes as $userId => $like) {
            if (!isset(server::$users[$userId])) {
                continue;
            }
            
            $model = \ORM::for_table(self::$likeTableName)->create();
            $model->game = $this->game->model->id;
            $model->user = $this->model->id;
            $model->like_user = server::$users[$userId]->model->id;
            $model->like = $like;   
            $model->save();
        }
        
    }
    

    private static function decodeParams($data) {
        $message = explode("::", $data);
        if (count($message) != 2) {
            return false;
        }
        $params = json_decode(trim($message[1]), true , 512, JSON_BIGINT_AS_STRING);
        if (!$params) {
            return false;
        }

        return array($message[0], $params);
    }
    

}
