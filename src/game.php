<?php

namespace socketServer;

class game {

    private static $maxUsers = 3;
    private static $minUsers = 2;
    private static $gameTableName = 'game';    
    private static $stepOneTime = 30;
    private static $stepTwoTime = 30;
    private static $stepThreeTime = 30;
    private static $sendTimeout = 5;
    private $startTime;
    private $lastSend = 0;
    private $update;
    public $step;
    public $model;
    public $users = array();

    public static function createOrFindGame(&$user) {
        if (!server::$games) {
            $game = new game($user);
            server::$games[$game->model->id] = $game;
            return;
        }

        foreach (server::$games as &$game) {
            //тут обработка поиска подходящих игр            
            if (count($game->users) < self::$maxUsers && $game->step == 1) {
                $game->addUser($user);
                return;
            }
        }

        $game = new game($user);
        server::$games[$game->model->id] = $game;
        return;
    }

    public function __construct(&$user) {
        \ORM::reset_db();
        $model = \ORM::for_table(self::$gameTableName)->create();
        $model->startUser = $user->model->id;
        $model->save();
        $this->model = $model;
        $this->addUser($user);
        $this->startTime = time();
        $this->step = 1;
    }

    public function addUser(&$user) {
        $user->game = &$this;
        $this->users[$user->model->userId] = &$user;
        logger::log("User #{$user->model->userId} add to game #{$this->model->id}", $user->connect, 2);
        $this->update = true;
        \ORM::reset_db();
        
    }

    public function deleteUser(&$user) {
        $this->update = true;
        logger::log("User #{$user->model->userId} disconect from game #{$this->model->id}", $user->connect, 2);
        unset($this->users[$user->model->userId]);
        unset(server::$connects[array_search($user->connect, server::$connects)]);
        unset(server::$users[$user->model->userId]);
        fclose($user->connect);

        if (!count($this->users)) {
            $this->closeGame();
        }
    }
    
    public function answer(&$user, $params) {
        $user->ansewrs[$params[1]['userId']] = $params[1]['myAnswer'];
        //$this->update = true;        
    }
    
    public function likeUserAnswer(&$user, $params) {
        $user->likes[$params[1]['userId']]= $params[1]['like']; 
        //$this->update = true; 
    }

    public function work() {
        switch ($this->step) {
            case 1:
                $this->stepOne();
                break;
            case 2:
                $this->stepTwo();
                break;
            case 3:
                $this->stepThree();
                break;
            case 4:
                $this->endGame();
                break;
            default :
                break;
        }
    }

    private function getUsersForStepOne() {
        $array = array();

        foreach ($this->users as &$user) {
            $array[] = array(
                'userId' => $user->model->userId,
                'name' => $user->model->name,
                'gender' => $user->model->gender,
                'birthday' => $user->model->birthday,
                'image1' => $user->model->image1,
            );
        }

        return $array;
    }

    private function stepOne() {
        if ($this->lastSend + self::$sendTimeout > time()) {
            return;
        }

        if ($this->update) {
            $data = $this->getUsersForStepOne();
            foreach ($this->users as &$user) {
                $messageArray = array(
                    "users" => $data
                );
                server::sendMessage($user->connect, 'preparation', $messageArray);
            }

            $this->update = false;
        }


        if (count($this->users) == self::$maxUsers) {
            $this->step = 2;
            $this->update = true;
            $this->startTime = time();
            logger::log("Game #{$this->model->id} step 2", false, 2);
        }

        if ($this->startTime + self::$stepOneTime < time() && count($this->users) < self::$minUsers) {
            $this->closeGame();
        } elseif ($this->startTime + self::$stepOneTime < time()) {
            $this->step = 2;
            $this->update = true;
            $this->startTime = time();
            logger::log("Game #{$this->model->id} step 2", false, 2);
        }

        $this->lastSend = time();
    }

    private function getUsersForStepTwo(&$currentUser) {
        $array = array();

        foreach ($this->users as &$user) {
            $myAnswer = '';
            
            
            if (isset($currentUser->ansewrs[$user->model->userId])) {
                $myAnswer = $currentUser->ansewrs[$user->model->userId];
            }
            
            
            $array[] = array(
                'userId' => $user->model->userId,
                'name' => $user->model->name,
                'gender' => $user->model->gender,
                'birthday' => $user->model->birthday,
                'image1' => $user->model->image1,
                'image2' => $user->model->image2,
                'image3' => $user->model->image3,
                'image4' => $user->model->image4,
                'image5' => $user->model->image5,
                'image6' => $user->model->image6,
                'question' => $user->question,
                'myAnswer' => $myAnswer
            );
        }

        return $array;
    }

    private function stepTwo() {
        if ($this->lastSend + self::$sendTimeout > time()) {
            return;
        }

        if ($this->update) {

            foreach ($this->users as &$user) {
                $messageArray = array(
                    "timer" => $this->startTime + self::$stepTwoTime - time(),
                    "users" => $this->getUsersForStepTwo($user)
                );
                server::sendMessage($user->connect, 'answerQuestions', $messageArray);
            }

            $this->update = false;
        }
        
        if ($this->startTime + self::$stepTwoTime < time()) {
            $this->step = 3;
            $this->update = true;
            $this->startTime = time();
            logger::log("Game #{$this->model->id} step 3", false, 2);
        }

        $this->lastSend = time();
    }
    
    private function getUsersForStepThree(&$currentUser) {
        $array = array();

        foreach ($this->users as &$user) {
            $answer = '';
            
            
            if (isset($user->ansewrs[$currentUser->model->userId])) {
                $answer = $user->ansewrs[$currentUser->model->userId];
            }
            
            
            $array[] = array(
                'userId' => $user->model->userId,
                'name' => $user->model->name,
                'gender' => $user->model->gender,
                'birthday' => $user->model->birthday,
                'image1' => $user->model->image1,
                'image2' => $user->model->image2,
                'image3' => $user->model->image3,
                'image4' => $user->model->image4,
                'image5' => $user->model->image5,
                'image6' => $user->model->image6,
                'question' => $user->question,
                'answer' => $answer
            );
        }

        return $array;
    } 
    
    private function stepThree() {
        if ($this->lastSend + self::$sendTimeout > time()) {
            return;
        }

        if ($this->update) {

            foreach ($this->users as &$user) {
                $messageArray = array(
                    "timer" => $this->startTime + self::$stepThreeTime - time(),
                    "users" => $this->getUsersForStepThree($user)
                );
                server::sendMessage($user->connect, 'choiceFavorite', $messageArray);
            }

            $this->update = false;
        }
        
        if ($this->startTime + self::$stepThreeTime < time()) {
            $this->step = 4;
        }

        $this->lastSend = time();
    }
    

    public function closeGame() {

        foreach ($this->users as &$user) {
            unset(server::$connects[array_search($user->connect, server::$connects)]);
            unset(server::$users[$user->model->userId]);
            fclose($user->connect);
        }

        unset(server::$games[$this->model->id]);
        logger::log("Game #{$this->model->id} close", false, 2);
    }
    
    private function endGame() {
        \ORM::reset_db();
        foreach ($this->users as &$user) {
            $user->saveQuestion(); 
        }
        
        foreach ($this->users as &$user) {
            $user->saveAnswerAndLikes(); 
        }
        
        foreach ($this->users as &$user) {
            
            
            $messageArray = array(
                "gameId" => $this->model->id
            );
            server::sendMessage($user->connect, 'endGame', $messageArray);
            unset(server::$connects[array_search($user->connect, server::$connects)]);
            unset(server::$users[$user->model->userId]);
            fclose($user->connect);
        }
        unset(server::$games[$this->model->id]);
        logger::log("Game #{$this->model->id} end", false, 2);
    }

}
